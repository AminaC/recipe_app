This is a recipe app developed in react js. 
It contains 2 web pages: the first is the landing page where I used SVG for 
design purposes followed by some animations.The second page is the main page
that uses recipes fetched from EDAMAM API. The list of searched recipes are
displayed on that page (their titles/images and ingredients).
I used react router to navigate between pages.
