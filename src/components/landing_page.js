import React from 'react';
import SvgImg from "./svg_component";
import LandingBtn from "./landing_button";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Recipes from "./Recipes";
import style from "./landing_page.css";


function LandingPage() {
  return (
    <div className="land">
      <SvgImg />
      <h1 claasName="title_land">Recipe Finder</h1>
      <p claasName="text_land">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
      <LandingBtn/>
    </div>

  );
}

export default LandingPage;
