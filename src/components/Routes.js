import React from 'react';
import{BrowserRouter, Switch, Route} from "react-router-dom";
import Recipes from "./Recipes";
import LandingPage from "./landing_page";


function Routes() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={LandingPage}/>

          <Route exact path="/Recipes" component={Recipes}/>

        </Switch>
      </BrowserRouter>
    </div>

  );
}

export default Routes;
