import React,{useState,useEffect} from 'react';
import RecipeComponent from "./Recipe";
import style from "./Recipes.css";

function Recipes() {
  const APP_KEY = "34160181466b0f12fbd59d6e2ac102e3";
  const APP_ID = "6844088a";
  const [recipes,setRecipes] = useState([]);
  const [query,setQuery] = useState("bread");
  const [search,setSearch] = useState("");

  useEffect(()=>{
     getData();
  },[query]);

  const getData = async() => {
    const dataItems = await fetch(`https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`);
    const data = await dataItems.json();
    setRecipes(data.hits);
    console.log(data.hits);
  }
  function updateSearch(event){
    setSearch(event.target.value);
    console.log(search);
  }

  function getSearch(event){
    event.preventDefault();
    setQuery(search);
  }

  return (
    <div>
       <section className="section1">
         <div className="icons">
           <i className="fal fa-hat-chef fa-6x hat"></i>
           <i className="fas fa-utensils-alt fa-6x spoon_fork"></i>
        </div>
         <form onSubmit={getSearch}>
           <input type="text" placeholder="Enter to find your recipe" className="searchIn" value={search} onChange={updateSearch}/>
               <button type="submit" className="searchBtn"><i className="fas fa-search search"></i></button>
        </form>

       </section>

       <div className="recipes">
         {recipes.map(recipe => (
          <RecipeComponent
          key={recipe.recipe.label}
          title={recipe.recipe.label}
          image={recipe.recipe.image}
          ingredients={recipe.recipe.ingredients}/>))}
      </div>

    </div>

  );
}

export default Recipes;
