import React from 'react';
import './svg_component.css';
import { ReactComponent as MyIcon } from "./land_img.svg";

function SvgImg() {
  return (
    <div>
      <MyIcon height="300px" width="400px"/>
    </div>
  );
}

export default SvgImg;
