import React from 'react';
import './landing_button.css';
import Recipes from "./Recipes";
import {Link, withRouter} from "react-router-dom";

function LandingBtn() {
  return (
   <div>
    <withRouter>
      <Link to="/Recipes">
        <button className="land_btn">
         See Our Recipes
        </button>
     </Link>
   </withRouter>
  </div>

  );
}

export default LandingBtn;
