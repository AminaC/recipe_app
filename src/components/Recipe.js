import React,{useState} from "react";
import style from "./Recipe.css";
import {Collapse,Button} from "reactstrap";


function RecipeComponent({title,image,ingredients}){
  const [show, setShow] = useState(false);

  return(
    <div className="Recipe">
      <h1 className="RecipeTitle">{title}</h1>
      <img src={image} className="RecipeImg" />
      <button value="btn" className="recipeBtn" onClick={() => setShow(!show)}>
      Click To See Recipe
      </button>
      {show && <ul className="ingredient-list">
         {ingredients && ingredients.map(ingredient =>(
           <li className="ingredients">{ingredient.text}</li>
         ))}
      </ul>}
    </div>



  );
}

export default RecipeComponent;
